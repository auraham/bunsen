# index_parser.py
from bs4 import BeautifulSoup as bs
import os

def is_jpeg(filename):
    """
    Returns True if filename is JPEG
    """
    
    filename, extension = os.path.splitext(filename)
    
    if extension in (".jpeg", ".jpg"):
        return True
        
    return False

if __name__ == "__main__":
    
    # extract jpeg "original" files 
    list_comics = []
    with open("Bunsen_files.xml") as file:
        
        lines = file.readlines()
        lines = [ line.replace("\n", "") for line in lines]
        lines = "".join(lines)
        soup = bs(lines, "lxml")
        
        for file in soup.find_all("file"):
            
            filename = file.get("name")
            source = file.get("source")
        
            # add file to list
            if is_jpeg(filename) and source == "original":
                list_comics.append(filename)
